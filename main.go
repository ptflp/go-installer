package main

import (
	"bytes"
	"fmt"
	"github.com/go-git/go-git/v5"
	"go/build"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"runtime"
	"strings"
)

const mentorMsg = "Сообщение ментору:"

func main() {
	args := os.Args
	if len(args) < 2 {
		args = append(args, "https://github.com/zulu82/-GO")
	}
	if len(args) != 2 {
		fmt.Println("error: wrong parameter count")
	}
	if !strings.Contains(args[1], "github.com") && !strings.Contains(args[1], "gitlab.com") {
		fmt.Println("error: link must contains github.com or gitlab.com")
		os.Exit(2)
	}
	projectURL := strings.ReplaceAll(args[1], "https://", "")
	projectURL = strings.ReplaceAll(projectURL, "http://", "")
	projectPaths := strings.Split(projectURL, "/")
	if len(projectPaths) != 3 {
		fmt.Printf("error: wrong url format %s", projectURL)
		os.Exit(2)
	}
	var correct bool
	regex := regexp.MustCompile("^[a-zA-Z0-9].+")
	for !correct {
		if !regex.MatchString(projectPaths[2]) {
			fmt.Printf("внимание кандидат использовал некорректное название проекта %s \n", projectPaths[2])
			projectPaths[2] = projectPaths[2][1:]
		} else {
			correct = true
		}
	}
	correct = false
	regex = regexp.MustCompile(".+[a-zA-Z0-9]$")
	for !correct {
		if !regex.MatchString(projectPaths[2]) {
			fmt.Printf("внимание кандидат использовал некорректное название проекта %s \n", projectPaths[2])
			projectPaths[2] = projectPaths[2][:len(projectPaths[2])]
		} else {
			correct = true
		}
	}
	if len(projectPaths[2]) < 2 {
		projectPaths[2] = "kata-calc"
	}
	goPath := os.Getenv("GOPATH")
	if goPath == "" {
		goPath = build.Default.GOPATH
	}
	projectPath := filepath.Join(goPath, "src", projectPaths[0], projectPaths[1], projectPaths[2])
	fmt.Println("проект кандидата будет находится по следующему пути:")
	fmt.Println(projectPath)
	if _, err := os.Stat(projectPath); !os.IsNotExist(err) {
		err = os.RemoveAll(projectPath)
		if err != nil {
			fmt.Println(err)
		}
	}

	err := os.MkdirAll(projectPath, os.ModePerm)
	if err != nil {
		fmt.Println(err)
		os.Exit(2)
	}
	_, err = git.PlainClone(projectPath, false, &git.CloneOptions{
		URL:      args[1],
		Progress: os.Stdout,
	})
	if err != nil && err.Error() != "repository already exists" {
		fmt.Println(err)
		os.Exit(2)
	}

	cmd := exec.Command("go", "fmt", "./...")
	cmd.Dir = projectPath
	cmd.Stderr = os.Stderr
	os.Setenv("GO111MODULE", "off")
	err = cmd.Run()
	if err != nil {
		fmt.Println("ошибка форматирования кода, задание не принимается")
		os.Exit(2)
	}
	os.Setenv("GO111MODULE", "auto")

	var count int
	err = filepath.Walk(projectPath, func(path string, info os.FileInfo, err error) error {
		if path == projectPath {
			return nil
		}
		if info.Name()[0] == '.' {
			return nil
		}
		if strings.Contains(path, "/.git") {
			return nil
		}
		if strings.Contains(path, "/.idea") {
			return nil
		}
		if err != nil {
			fmt.Println(err)
			return err
		}
		if info.IsDir() {
			return nil
		}
		if info.Name() == "go.mod" {
			cmd = exec.Command("go", "mod", "tidy")
			cmd.Stderr = os.Stderr
			cmd.Stdout = os.Stdout
			fileDir, _ := filepath.Split(path)
			cmd.Dir = fileDir
			err = cmd.Run()
			if err != nil {
				fmt.Println(err)
				os.Exit(2)
			}
		}
		if !strings.Contains(info.Name(), ".go") {
			return nil
		}
		data, _ := os.ReadFile(path)
		src := string(data)
		if strings.Contains(src, "package main") && strings.Contains(src, "func main() {") {
			binName := projectPaths[1] + "-calc"
			if runtime.GOOS == "windows" {
				binName = projectPaths[1] + "-calc.exe"
			}
			if count > 0 {
				binName = fmt.Sprintf(binName+"%v", count)
			}
			outPath := filepath.Join(goPath, "bin", binName)
			cmd = exec.Command("go", "build", "-o", outPath)
			fileDir, _ := filepath.Split(path)
			cmd.Dir = fileDir
			var b bytes.Buffer
			cmd.Stderr = &b
			err = cmd.Run()
			if err != nil {
				stdErr := b.String()
				os.Stderr.WriteString(stdErr)
				if strings.Contains(stdErr, "main redeclared in this block") {
					fmt.Println(mentorMsg, "попросить удалить лишний func main")
				}
				return err
			}
			fmt.Println("исполняемый файл вы найдете по следующему пути:")
			fmt.Println(outPath)
			count++
		}
		return nil
	})
	if err != nil {
		fmt.Println(err)
	}
}
